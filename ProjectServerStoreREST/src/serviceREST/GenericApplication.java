package serviceREST;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("resources")
public class GenericApplication extends Application {
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();

        // Register root resources.
        classes.add(LotREST.class);
        classes.add(WarehouseREST.class);
        classes.add(ProductREST.class);
        classes.add(CorsFilter.class);
        // Register provider classes.

        return classes;
    }
}
