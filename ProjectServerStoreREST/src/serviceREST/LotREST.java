package serviceREST;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import model.LotDTO;

import service.LotService;

@Path("api/v1/Lots")
public class LotREST {
    
    private LotService lotService;
    
    public LotREST() {
        this.lotService = LotService.getInstance();
    }

    @GET
    @Produces("application/json")
    @Path("getAll")
    public List<LotDTO> getAllLots() {
        return lotService.getAllLots();
    }

    @GET
    @Produces("application/json")
    @Path("findByStock")
    @SuppressWarnings("oracle.jdeveloper.webservice.rest.broken-resource-warning")
    public List<LotDTO> findByStockLikeLots(@QueryParam("stock") Integer stock) throws Exception {
        return lotService.findByStockLikeLots(stock);
    }

    @GET
    @Produces("application/json")
    @Path("find")
    @SuppressWarnings("oracle.jdeveloper.webservice.rest.broken-resource-warning")
    public LotDTO findLot(@QueryParam("id") Long id) throws Exception {
        return lotService.findLot(id);
    }

    @POST
    @Consumes("application/json")
    @Path("save")
    public void saveLot(LotDTO lot, @QueryParam("code") Integer code, @QueryParam("id") Integer id) {
        lotService.saveLot(lot, code, id);
    }

    @PUT
    @Consumes("application/json")
    @Path("update")
    public void updateLot(@QueryParam("id") Long id, LotDTO lot) throws Exception {
        lotService.updateLot(id, lot);
    }

    @DELETE
    @Consumes("application/json")
    @Path("remove")
    public void removeLot(@QueryParam("id") Long id) throws Exception {
        lotService.removeLot(id);
    }
    
}
