package serviceREST;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import model.LotDTO;
import model.ProductDTO;

import service.ProductService;

@Path("api/v1/Products")
public class ProductREST {
    
    private ProductService productService;
    
    public ProductREST() {
        this.productService = ProductService.getInstance();
    }

    @GET
    @Produces("application/json")
    @Path("getAllProducts")
    public List<ProductDTO> getAllProducts() {
        return productService.getAllProducts();
    }

    @GET
    @Produces("application/json")
    @Path("getAllByNameLikeProduct")
    @SuppressWarnings("oracle.jdeveloper.webservice.rest.broken-resource-warning")
    public List<ProductDTO> getAllByNameLikeProducts(@QueryParam("name") String name) throws Exception {
        return productService.getAllByNameLikeProducts(name);
    }

    @GET
    @Produces("application/json")
    @Path("findLotsProduct")
    @SuppressWarnings("oracle.jdeveloper.webservice.rest.broken-resource-warning")
    public List<LotDTO> findLotsProduct(@QueryParam("code") Integer code) throws Exception {
        return productService.findLotsProduct(code);
    }

    @GET
    @Produces("application/json")
    @Path("findProduct")
    @SuppressWarnings("oracle.jdeveloper.webservice.rest.broken-resource-warning")
    public ProductDTO findProduct(@QueryParam("code") Integer code) throws Exception {
        return productService.findProduct(code);
    }

    @POST
    @Consumes("application/json")
    @Path("saveProduct")
    public void saveProduct(ProductDTO product) {
        productService.saveProduct(product);
    }

    @PUT
    @Consumes("application/json")
    @Path("updateProduct")
    public void updateProduct(@QueryParam("code") Integer code, ProductDTO product) throws Exception {
        productService.updateProduct(code, product);
    }

    @DELETE
    @Consumes("application/json")
    @Path("removeProduct")
    public void removeProduct(@QueryParam("code") Integer code) throws Exception {
        productService.removeProduct(code);
    }
    
}
