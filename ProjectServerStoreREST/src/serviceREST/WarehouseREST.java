package serviceREST;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import model.LotDTO;
import model.WarehouseDTO;

import service.WarehouseService;

@Path("serviceREST")
public class WarehouseREST {
    
    private WarehouseService warehouseService;
    
    public WarehouseREST() {
        this.warehouseService = WarehouseService.getInstance();
    }

    @GET
    @Produces("application/json")
    @Path("getAllWarehouses")
    public List<WarehouseDTO> getAllWarehouses() {
        return warehouseService.getAllWarehouse();
    }

    @GET
    @Produces("application/json")
    @Path("findLotsWarehouse")
    @SuppressWarnings("oracle.jdeveloper.webservice.rest.broken-resource-warning")
    public List<LotDTO> findLotsWarehouse(@QueryParam("id") Integer id) throws Exception {
        return warehouseService.findLotsWarehouse(id);
    }

    @GET
    @Produces("application/json")
    @Path("findWarehouse")
    @SuppressWarnings("oracle.jdeveloper.webservice.rest.broken-resource-warning")
    public WarehouseDTO findWarehouse(@QueryParam("id") Integer id) throws Exception {
        return warehouseService.findWarehouses(id);
    }    
    
}
