package service;

import dao.ProductDAO;

import dao.WarehouseDAO;

import java.util.List;

import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Lot;
import model.LotDTO;
import model.ProductDTO;
import model.Warehouse;
import model.WarehouseDTO;

public class WarehouseService {
    
    private static WarehouseService warehouseService;
    
    private final EntityManagerFactory emf;
        
    private final EntityManager em;
    
    private final WarehouseDAO warehouseDao;
    
    private WarehouseService() {
        super();
        this.emf = Persistence.createEntityManagerFactory("ChainStore");
        this.em = emf.createEntityManager();
        this.warehouseDao = new WarehouseDAO();
    }

    @SuppressWarnings("oracle.jdeveloper.java.nested-assignment")
    public static WarehouseService getInstance() {
        return warehouseService == null ? warehouseService = new WarehouseService() : warehouseService;
    }
    
    public List<WarehouseDTO> getAllWarehouse() {
        warehouseDao.setEntityManager(em);
        List<WarehouseDTO> warehouses = warehouseDao.findAll()
                                              .stream()
                                              .map(WarehouseDTO::new)
                                              .collect(Collectors.toList());
        return warehouses;
    }
        
    public List<LotDTO> findLotsWarehouse(Integer id) throws Exception {
        warehouseDao.setEntityManager(em);
        List<LotDTO> lotsWarehouse = warehouseDao.findLots(id)
                                              .stream()
                                              .map(LotDTO::new)
                                              .collect(Collectors.toList());
        return lotsWarehouse;
    }
        
    public WarehouseDTO findWarehouses(Integer id) throws Exception {
        warehouseDao.setEntityManager(em);
        return new WarehouseDTO(warehouseDao.find(id));
    }    

}
