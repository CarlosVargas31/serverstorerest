package service;

import dao.LotDAO;

import java.util.List;

import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Lot;
import model.LotDTO;
import model.ProductDTO;

public class LotService {
    
    private static LotService lotService;
    
    private final EntityManagerFactory emf;
        
    private final EntityManager em;
    
    private final LotDAO lotDao;
    
    private LotService() {
        super();
        this.emf = Persistence.createEntityManagerFactory("ChainStore");
        this.em = emf.createEntityManager();
        this.lotDao = new LotDAO();
    }
    
    /**
     * @return
     */
    @SuppressWarnings("oracle.jdeveloper.java.nested-assignment")
    public static LotService getInstance() {
        return lotService == null ? lotService = new LotService() : lotService;
    }
    
    public List<LotDTO> getAllLots() {
        lotDao.setEntityManager(em);
        List<LotDTO> lots = lotDao.findAll()
                                  .stream()
                                  .map(LotDTO::new)
                                  .collect(Collectors.toList());
        return lots;
    }
        
    public List<LotDTO> findByStockLikeLots(Integer stock) throws Exception {
        lotDao.setEntityManager(em);
        List<LotDTO> lots = lotDao.findByStockLike(stock)
                                  .stream()
                                  .map(LotDTO::new)
                                  .collect(Collectors.toList());
        return lots;
    }
        
    public LotDTO findLot(Long id) throws Exception {
        lotDao.setEntityManager(em);
        return new LotDTO(lotDao.find(id));
    }
    
    public void saveLot(LotDTO lot, Integer code, Integer id) {
        lotDao.setEntityManager(em);
        lotDao.save(new Lot(lot), code, id);
    }
        
    public void updateLot(Long id, LotDTO lot) throws Exception {
        lotDao.setEntityManager(em);
        lotDao.update(id, new Lot(lot));
    }
        
    public void removeLot(Long id) throws Exception {
        lotDao.setEntityManager(em);
        lotDao.remove(id);
    }
    
}
