package service;

import dao.ProductDAO;


import java.util.List;

import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Lot;
import model.LotDTO;
import model.Product;
import model.ProductDTO;

public class ProductService {
    
    private static ProductService productService;
    
    private final EntityManagerFactory emf;
        
    private final EntityManager em;
    
    private final ProductDAO productDao;
    
    private ProductService() {
        super();
        this.emf = Persistence.createEntityManagerFactory("ChainStore");
        this.em = emf.createEntityManager();
        this.productDao = new ProductDAO();
    }

    /**
     * @return
     */
    @SuppressWarnings("oracle.jdeveloper.java.nested-assignment")
    public static ProductService getInstance() {
        return productService == null ? productService = new ProductService() : productService;
    }
    
    public List<ProductDTO> getAllProducts() {
        productDao.setEntityManager(em);
        List<ProductDTO> products = productDao.findAll()
                                              .stream()
                                              .map(ProductDTO::new)
                                              .collect(Collectors.toList());
        return products;
    }
        
    public List<ProductDTO> getAllByNameLikeProducts(String name) throws Exception {
        productDao.setEntityManager(em);
        List<ProductDTO> products = productDao.findByNameLike(name)
                                              .stream()
                                              .map(ProductDTO::new)
                                              .collect(Collectors.toList());
        return products;
    }
        
    public List<LotDTO> findLotsProduct(Integer code) throws Exception {
        productDao.setEntityManager(em);
        List<LotDTO> lotsProduct = productDao.findLots(code)
                                              .stream()
                                              .map(LotDTO::new)
                                              .collect(Collectors.toList());
        return lotsProduct;
    }
        
    public ProductDTO findProduct(Integer code) throws Exception {
        productDao.setEntityManager(em);
        return new ProductDTO(productDao.find(code));
    }
        
    public void saveProduct(ProductDTO product) {
        productDao.setEntityManager(em);
        productDao.save(new Product(product));
    }
        
    public void updateProduct(Integer code, ProductDTO product) throws Exception {
        productDao.setEntityManager(em);
        productDao.update(code, new Product(product));
    }
        
    public void removeProduct(Integer code) throws Exception {
        productDao.setEntityManager(em);
        productDao.remove(code);
    }
    
}
