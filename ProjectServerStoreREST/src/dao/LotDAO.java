package dao;

import java.util.List;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;

import javax.persistence.criteria.CriteriaQuery;

import javax.persistence.criteria.Root;

import model.Lot;
import model.Product;
import model.Warehouse;

public class LotDAO {
    
    private EntityManager em;
        
        private final Class entityClass;
            
        public LotDAO() {
            entityClass = Lot.class;
        }
        
        public EntityManager getEntityManager() {
            return em;
        }
        
        public void setEntityManager(EntityManager em) {
            this.em = em;
        }
        
        public void save(Lot lot, Integer code, Integer id) throws EntityExistsException {
            try {
                getEntityManager().getTransaction().begin();
                Product product = getEntityManager().find(Product.class, code);
                Warehouse warehouse = getEntityManager().find(Warehouse.class, id);
                product.addLots(lot);
                warehouse.addLots(lot);
                getEntityManager().persist(lot);
                getEntityManager().getTransaction( ).commit( );
            } catch (Exception e) {
                if (getEntityManager().getTransaction().isActive()) {
                    getEntityManager().getTransaction().rollback();
                }
                
                throw new EntityExistsException(e);
            }
        }

        public void update(Long id, Lot lot) throws Exception {
            Lot lotToUpdate = find(id);
            lotToUpdate.setWarehouse(lot.getWarehouse());
            lotToUpdate.setProduct(lot.getProduct());
            lotToUpdate.setStock(lot.getStock());
            getEntityManager().getTransaction().begin();
            getEntityManager().merge(lotToUpdate);
            getEntityManager().getTransaction( ).commit( );
        }

        public void remove(Long id) throws Exception {
            Lot lotToDelete = find(id);
            getEntityManager().getTransaction().begin();
            getEntityManager().remove(getEntityManager().merge(lotToDelete));
            getEntityManager().getTransaction( ).commit( );
        }

        public Lot find(Long id) throws Exception {
            Lot lot = (Lot) getEntityManager().find(entityClass, id);
            
            if (lot == null) {
                throw new Exception("Lot not found..");
            }
            
            return lot;
        }
        
        public List<Lot> findByStockLike(Integer stock) throws Exception {
            String query = "SELECT * FROM LOTS WHERE STOCK = "+ stock;
            Query q = getEntityManager().createNativeQuery(query, entityClass);
            List<Lot> result = q.getResultList();
            
            if (result.isEmpty()) {
                throw new Exception("Lots wasn't found by the filtered parameter..");
            }
            
            return result;
        }

        public List<Lot> findAll() {
            CriteriaBuilder cb = getEntityManager().getCriteriaBuilder();
            CriteriaQuery<Lot> cq = cb.createQuery(entityClass);
            Root<Lot> root = cq.from(entityClass);
            cq.select(root).orderBy(cb.asc(root.get("id")));
            return getEntityManager().createQuery(cq).getResultList();
        }
    
}
