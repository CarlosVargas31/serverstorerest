package model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "WarehouseDTO")
public class WarehouseDTO implements Serializable {
    
    @SuppressWarnings("compatibility:-7288766174981416768")
    private static final long serialVersionUID = 5722695847689939284L;

    private Integer id;

    private String name;

    private String address;
    
    public WarehouseDTO() { }
    
    public WarehouseDTO(Warehouse warehouse) {
        this.id = warehouse.getId();
        this.name = warehouse.getName();
        this.address = warehouse.getAddress();
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
