package model;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lots")
@SuppressWarnings("oracle.jdeveloper.ejb.entity-class-auto-id-gen")
public class Lot {
    
    @Id
    @Column(name = "id")
    private Long id;
        
    @ManyToOne(optional = false, cascade = {
        CascadeType.DETACH, CascadeType.REFRESH
    })
    @JoinColumn(name = "warehouse_id", nullable = false, foreignKey = 
            @ForeignKey(name = "fk_lots_warehouses")
    )
    private Warehouse warehouse;
        
    @ManyToOne(optional = false, cascade = {
        CascadeType.DETACH, CascadeType.REFRESH
    })
    @JoinColumn(name = "product_code", nullable = false, foreignKey = 
        @ForeignKey(name = "fk_lots_product")
    )
    private Product product;
        
    @Column(name = "stock", nullable = false)
    private Integer stock;

    public Lot() { }

    public Lot(Long id) {
        this.id = id;
    }

    public Lot(Integer stock) {
        this.stock = stock;
    }

    public Lot(Long id, Integer stock) {
        this.id = id;
        this.stock = stock;
    }

    public Lot(Warehouse warehouse, Product product, Integer stock) {
        this.warehouse = warehouse;
        this.product = product;
        this.stock = stock;
    }
    
    public Lot(LotDTO lotDTO) {
        this.id = lotDTO.getId();
        this.stock = lotDTO.getStock();
        
        if (lotDTO.getWarehouse() != null) {
            this.warehouse = new Warehouse(lotDTO.getWarehouse());    
        } else {
            this.warehouse = null;
        }
        
        if (lotDTO.getProduct() != null) {
            this.product = new Product(lotDTO.getProduct());
        } else {
            this.product = null;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
        
    public String[] genericHeaderTable() {
        String[] headerData = new String[4];
        headerData[0] = "Id";
        headerData[1] = "Warehouse";
        headerData[2] = "Product";
        headerData[3] = "Stock";
        return headerData;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
            }
        final Lot other = (Lot) obj;
        return Objects.equals(this.id, other.id);
    }
    
    @Override
    public String toString() {
        return "Lot{" + "id=" + id + ", warehouse=" + warehouse.getName() +
               ", product=" + product.getName() + ", stock=" + stock + '}';
    }
    
}
