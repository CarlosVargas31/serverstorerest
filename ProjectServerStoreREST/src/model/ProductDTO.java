package model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ProductDTO")
public class ProductDTO implements Serializable {
    
    @SuppressWarnings("compatibility:2457333777266411640")
    private static final long serialVersionUID = -4630247296513554709L;

    private int code;
    
    private String name;
    
    private double price;
    
    public ProductDTO() { }
    
    public ProductDTO(int code) {
        this.code = code;
    }

    public ProductDTO(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public ProductDTO(int code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }
    
    public ProductDTO(Product product) {
        this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
    }
    
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
