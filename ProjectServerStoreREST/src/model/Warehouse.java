package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "warehouses", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name", name = "uk_warehouses_name"),
        @UniqueConstraint(columnNames = "address", name = "uk_warehouses_address")
})
@SuppressWarnings("oracle.jdeveloper.ejb.entity-class-auto-id-gen")
public class Warehouse {

    @Id
    @Column(name = "id")
    private Integer id;
        
    @Column(name = "name", length = 150, nullable = false)
    private String name;
        
    @Column(name = "address", length = 255, nullable = false)
    private String address;
        
    @OneToMany(mappedBy = "warehouse", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Lot> lots;

    public Warehouse() { }

    public Warehouse(String name, String address) {
        this.name = name;
        this.address = address;
    }
    
    public Warehouse(WarehouseDTO warehouseDTO) {
        this.id = warehouseDTO.getId();
        this.name = warehouseDTO.getName();
        this.address = warehouseDTO.getAddress();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<Lot> getLots() {
        return lots;
    }

    public void setLots(List<Lot> lots) {
        this.lots = lots;
    }
        
    public void addLots(Lot lot) {
        if (lots.isEmpty()) {
            lots = new ArrayList<>();
        }
            
        lots.add(lot);
        lot.setWarehouse(this);
    }
        
    public String[] genericHeaderTable() {
        String[] headerData = new String[3];
        headerData[0] = "Id";
        headerData[1] = "Name";
        headerData[2] = "Address";
        return headerData;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Warehouse other = (Warehouse) obj;
        return Objects.equals(this.id, other.id);
    }
        
    @Override
    public String toString() {
        return name;
    }

}
