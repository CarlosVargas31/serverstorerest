package model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "LotDTO")
public class LotDTO implements Serializable {
    
    @SuppressWarnings("compatibility:-3313489153974856496")
    private static final long serialVersionUID = -5605132728307992747L;

    private Long id;

    private WarehouseDTO warehouse;

    private ProductDTO product;

    private Integer stock;
    
    public LotDTO() { }
    
    public LotDTO(Long id) {
        this.id = id;
    }

    public LotDTO(Integer stock) {
        this.stock = stock;
    }

    public LotDTO(Long id, Integer stock) {
        this.id = id;
        this.stock = stock;
    }

    public LotDTO(Warehouse warehouse, Product product, Integer stock) {
        this.warehouse = new WarehouseDTO(warehouse);
        this.product = new ProductDTO(product);
        this.stock = stock;
    }
    
    public LotDTO(Lot lot) {
        this.id = lot.getId();
        this.stock = lot.getStock();
        this.warehouse = new WarehouseDTO(lot.getWarehouse());
        this.product = new ProductDTO(lot.getProduct());    
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public WarehouseDTO getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(WarehouseDTO warehouse) {
        this.warehouse = warehouse;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
    
}
