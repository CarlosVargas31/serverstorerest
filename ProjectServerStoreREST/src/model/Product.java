package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;

@Entity
@Table(name = "products", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name", name = "uk_products_name")
})
@SuppressWarnings("oracle.jdeveloper.ejb.entity-class-auto-id-gen")
public class Product {
    
    @Id
    @Column(name = "code", updatable = false)
    private int code;
    
    @Column(name = "name", length = 40, nullable = false)
    private String name;
    
    @Column(name = "price", columnDefinition = "NUMERIC(19, 2) NOT NULL")
    private double price;
    
    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = {
        CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH
    })
    private List<Lot> lots;

    public Product() { }

    public Product(int code) {
        this.code = code;
    }

    public Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public Product(int code, String name, double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }
    
    public Product(ProductDTO productDTO) {
        this.code = productDTO.getCode();
        this.name = productDTO.getName();
        this.price = productDTO.getPrice();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<Lot> getLots() {
        if (lots.isEmpty()) {
            lots = new ArrayList<>();
        }
        
        return lots;
    }

    public void setLots(List<Lot> lots) {
        this.lots = lots;
    }
    
    public void addLots(Lot lot) {
        if (lots.isEmpty()) {
            lots = new ArrayList<>();
        }
        
        lots.add(lot);
        lot.setProduct(this);
    }
    
    public String[] genericHeaderTable() {
        String[] headerData = new String[3];
        headerData[0] = "Code";
        headerData[1] = "Name";
        headerData[2] = "Price";
        return headerData;
    }

    @Override
    public String toString() {
        return name;
    }
        
}
